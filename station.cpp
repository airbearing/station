/**************************************************************************
 *  Air Bearing - Station function suite
 *  David Criado Pernia
 *  2019
 *
 *  HARDWARE
 *  ------------------------------------------
 *    Adafruit Feather M0 RFM69HCW     Radio txr
 *
 *  CPP FILE CONTENTS
 *  ------------------------------------------
 *  1. BEGIN
 *  2. TELECOMMANDS (LISTEN)
 *  3. OPERATE
 *
 **************************************************************************/

#include "station.h"

/**************************************************************************
 *  Inherited constructors
 *  Solved thanks to: https://goo.gl/H4yL13
 **************************************************************************/

// Singleton instance of the radio driver
RH_RF69 rf69(RFM69_CS, RFM69_INT);
// Class to manage message delivery and receipt, using the driver above
RHReliableDatagram rf69_manager(rf69, MY_ADDRESS);

/**************************************************************************
 *  CLASS
 **************************************************************************/

// Station class constructor
Station::Station(){}

/**************************************************************************
 *  BEGIN functions
 **************************************************************************/

// Begin station
void Station::begin()
{
    // Start USB Serial connection
    beginSerial();

    // Start Feather M0
    beginFeather();

    // Start Radio
    beginRadio();

    Serial.println("Station booted successfully");
}

// Begin Serial port
void Station::beginSerial()
{
    Serial.begin(BAUDRATE ? BAUDRATE : 115200);
    delay(1000);
    Serial.println("Booting station...");
    Serial.println("[1/2]\tSerial inited");
}

// Begin Feather M0
void Station::beginFeather()
{
    pinMode     (LED,       OUTPUT);
    pinMode     (RFM69_RST, OUTPUT);
    digitalWrite(RFM69_RST, LOW);
}

// Begin radio transceiver
bool Station::beginRadio()
{
    // Manual reset
    blink(RFM69_RST, 1, false);

    // Start sensor
    if (!rf69_manager.init()) return false;
    Serial.println("\tInited RFM69 radio. Setting up radio...");

    // Defaults 434MHz, GFSK_Rb250Fd250, +13dbM
    if (!rf69.setFrequency(RF69_FREQ)) return false;

    // Range from 14-20 for power, 2nd arg must be true for 69HCW
    rf69.setTxPower(20, true);

    // Key same as server
    uint8_t key[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                     0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08}; 
    rf69.setEncryptionKey(key);

    Serial.println("[2/2]\tRadio configured and ready");
    return true;
}

/**************************************************************************
 *  2. TELECOMMANDS (LISTEN)
 **************************************************************************/

// Listen to telecommand via Serial
void Station::listen()
{
    // Notify that a new command has come!
    if (listenSerial()) _new_command = true;
}

// Listen if there is any telecommand via Serial
bool Station::listenSerial()
{
    // If there's any serial available, read it:
    while (Serial.available() > 0)
    {
        // Get command head, body and options
        uint8_t head = Serial.parseInt();
        uint8_t body = Serial.parseInt();
        uint8_t opts = Serial.parseInt();

        // Discard "0,0,0"
        if (head + body + opts == 0) return false;

        // Store command
        _cmd_head = head;
        _cmd_body = body;
        _cmd_opts = opts;
        return true;
    }
    return false;
}

/**************************************************************************
 *  3 OPERATE
 **************************************************************************/

// Transmit new command and receive RF data
void Station::operate()
{
    if (_new_command) transmitCommand();
    receive();
}

// Transmit to RF incoming commands from Serial
void Station::transmitCommand()
{
    // Okay, system have read the notification, now shut up
    _new_command = false;

    // Join strings
    String _cmd_str = command2string();

    // Send command to Platform
    transmit(_cmd_str);

    delay(100);
}

// Send message by Radio
void Station::transmit(String message)
{
    if (message.length() == 0) return;
    char _len = message.length();

    char radiopacket[_len+1];
    message.toCharArray(radiopacket, _len+1);

    // Send a message to the DESTINATION!
    if (!rf69_manager.sendtoWait((uint8_t *)radiopacket, _len+2, DEST_ADDRESS))
    {
        Serial.print("[!ACK]\t");
        Serial.println(message);
        return;
    }

    // // Now wait for a reply from the server
    // uint8_t buffer_len = sizeof(_radio_buffer);
    // uint8_t from_device;
    // if (rf69_manager.recvfromAckTimeout(_radio_buffer, &buffer_len, 200, &from_device))
    // {
    //     _radio_buffer[buffer_len] = 0; // zero out remaining string
    //     Serial.print("[ACK]\t");
    //     Serial.println(message);
    //     return;
    // }
    Serial.print("[ACK]\t");
    Serial.println(message);
}


// Get message from Radio
void Station::receive()
{
    // Exit if no data incoming
    if (!rf69_manager.available())
        return;

    // Wait for a message addressed to us from the client
    uint8_t len = sizeof(_radio_buffer);
    uint8_t from_device;

    // Exit if data not received
    if (!rf69_manager.recvfromAckTimeout(_radio_buffer, &len, 100, &from_device))
        return;

    _radio_buffer[len] = 0; // zero out remaining string

    // // Send a reply back to the originator client
    // uint8_t _ack[] = "A";
    // if (!rf69_manager.sendtoWait(_ack, sizeof(_ack), from_device))
    //     Serial.print("[!ACK]\t");
    
    // Prompt message through Serial
    Serial.println((char *)_radio_buffer);
}

/**************************************************************************
 *  AUX
 **************************************************************************/

// Alert of error by blinking LED and prompting message
void Station::alert(String message, uint8_t loops)
{
    if (message.length() > 0) Serial.println(message);
    while (true)
    {
        for (uint8_t i = 0; i < loops; i++)
        {
            blink(LED, 100, false);
        }
        delay(1000 - 100 * 2 * loops);
    }
}

// Blink LED or reset device by pulse through RST PIN
void Station::blink(uint8_t pin, uint8_t delta, bool inverted)
{
    if (inverted)
    {
        digitalWrite(pin, LOW);
        delay(delta);
        digitalWrite(pin, HIGH);
        delay(delta);
    }
    else
    {
        digitalWrite(pin, HIGH);
        delay(delta);
        digitalWrite(pin, LOW);
        delay(delta);
    }
}

// Get "h,b,o" string from stored command
String Station::command2string()
{
    String s0;
    s0 = String(_cmd_head);   s0.concat(",");
    s0.concat(_cmd_body);     s0.concat(",");
    s0.concat(_cmd_opts);
    return s0;
}
