/*  Station library for the Air Bearing.
 *  Developed for IDR - UPM (2019).
 *  Author: David Criado Pernia
 * 
 *  Library that is used to operate the monitoring
 *  station subsystem, comprised by an Adafruit
 *  Feather M0 RFM69HCW microcontroller + transceiver.
 * 
 *  File contains:
 *  - Dependencies to external libraries
 *  - Station constants
 *  - Inherited external constructors
 *  - Station class header
 */

// Prevent double declaration
#ifndef __STATION_H__
#define __STATION_H__


/* 
 * Dependencies to external libraries.
 */

// Arduino
#include <SPI.h>                // Internal SPI Arduino library

// Third-party
#include <RH_RF69.h>            // More info: https://goo.gl/ZeEy5k
#include <RHReliableDatagram.h> // More info: https://goo.gl/Ks9rfE

/* 
 * Constants
 */

///////// Transceiver parameters

#define RF69_FREQ (433.0)       //!< Transceiver Frequency [Hz]

#define DEST_ADDRESS    (2)     //!< Platform: Where to send packets to
#define MY_ADDRESS      (1)     //!< Station transceiver address

///////// Feather M0 w/Radio

#define RFM69_CS        (8)     //!< Feather M0 pin to transceiver SPI chip select pin
#define RFM69_INT       (3)     //!< Feather M0 pin to transceiver SPI interrupt pin
#define RFM69_RST       (4)     //!< Feather M0 pin to transceiver SPI reset pin
#define LED             (13)    //!< Feather M0 LED pin

//! Singleton instance of the radio driver
/*! Inherited external constructor.
 *  Solved thanks to: https://goo.gl/H4yL13
 * \param[in] slaveSelectPin the Arduino pin number of the output
 * to use to select the RF69 before accessing it. Defaults to SS.
 * \param[in] interruptPin Interrupt Pin number that is connected
 * to the RF69 DIO0 interrupt line. Defaults to 2.
 * \param[in] spi Pointer to the SPI interface object to use. 
 *  Defaults to the standard Arduino hardware SPI interface.
 */
extern RH_RF69 rf69;
//! Class to manage message delivery and receipt, using the driver above
/*! Inherited external constructor.
 *  Solved thanks to: https://goo.gl/H4yL13
 * \param[in] driver The RadioHead driver to use to transport messages.
 * \param[in] thisAddress The address to assign to this node. Defaults to 0
 */
extern RHReliableDatagram rf69_manager;

/*! \brief Station class header.
 *
 *  Used to operate the monitoring
 *  station subsystem, comprised by an Adafruit
 *  Feather M0 RFM69HCW microcontroller + transceiver.
 * 
 *  Contains begin, telecommands, and
 *  operation functionalities.
 */
class Station
{
  public:
    //! Class constructor
    Station                     (void);

    /////////////////////////////////////////////////

    //! Begin all station devices
    void begin                  (void);
    //! Configure Feather M0 board
    void beginFeather           (void);
    //! Begin embedded RFM69HCW transceiver
    /*!
     *  Inits transceiver and sets frequency,
     *  transmitting power (dBm), and packets
     *  encryption key.
     *  \return true if inited successfully
     */
    bool beginRadio             (void);
    //! Begin Serial USB interface
    /*! Allows receiving telecommands through
     *  the serial port.
     */
    void beginSerial            (void);

    /////////////////////////////////////////////////

    //! Listen for incoming commands
    void listen                 (void);
    //! Listen commands through Serial port
    /*! \return true if command arrived */
    bool listenSerial           (void);
    //! Interpret incoming commands
    void interpretCommand       (void);

    /////////////////////////////////////////////////

    //! Operate Station subsystem
    void operate                (void);
    //! Prepare and send command through RF to Platform
    void transmitCommand        (void);

    /////////////////////////////////////////////////

    //! USB Serial baudrate
    const int32_t BAUDRATE     = 115200;
  private:
    
    //! Send message through RF to Platform
    void transmit               (
                                      String message = "", //!< Message to transmit via RF
                                );
    //! Receive message through RF from Platform
    void receive                (void);

    /////////////////////////////////////////////////

    //! Alert of error by blinking LED and prompting message
    /*! ### LED blink error codes
     * - **Radio begin failed**: 1 blink per second
     */
    void alert                  (
                                  String message = "", //!< Alert to transmit via Serial USB
                                  uint8_t loops = 1    //!< Loops that encode LED blinking
                                );
    //! Blink LED or reset device by pulse through RST PIN
    void blink                  (
                                  uint8_t pin = LED,    //!< Target pin to trigger
                                  uint8_t delta = 10,   //!< Interval, in milliseconds
                                  bool inverted = false //!< Invert to first low and then high
                                );
        
    /////////////////////////////////////////////////

    //! Get "head,body,opts" string from stored command
    String command2string       (void);

    /////////////////////////////////////////////////

    // Listen and interpret commands
    uint8_t   _cmd_head,                              //!< Stores first parameter from incoming command 
              _cmd_body,                              //!< Stores second parameter from incoming command 
              _cmd_opts;                              //!< Stores third parameter from incoming command 
    bool      _new_command;                           //!< Notifies change to active operational mode

    // Transmit RF
    uint8_t   _radio_buffer[RH_RF69_MAX_MESSAGE_LEN];  //!< Prepares radio buffer to transmit
};
#endif
