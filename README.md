# IDR UPM Air Bearing

Monitoring station library for the Air Bearing. Developed for IDR - UPM (2019).

Library that is used to operate the monitoring station subsystem, comprised by an Adafruit Feather M0 RFM69HCW microcontroller + transceiver.

## Contents

- [Usage](#usage)
- [Dependencies](#dependencies)
- [Docs](#docs)
- [Repository structure](#structure)
- [Authors](#authors)
- [References](#references)

## Usage

1. Install [Arduino IDE](https://www.arduino.cc/en/Main/Software)
2. Clone or download this `Station` repository into the [default directory for the Arduino libraries](https://www.arduino.cc/en/guide/libraries) `~/Documents/Arduino/libraries/`.
3. Install [Feather board manager](https://learn.adafruit.com/adafruit-feather-m0-radio-with-rfm69-packet-radio/setup) for the Arduino IDE.
4. Install external libraries. [Check how](https://www.arduino.cc/en/guide/libraries).
5. Compile example into Teensy

## Dependencies

### Libraries installed with Arduino IDE

- [**SPI.h**](https://www.arduino.cc/en/reference/SPI): SPI coms between Feather M0 and embedded RFM69HCW

### External libraries

- [**RH_RF69.h**](https://goo.gl/ZeEy5k): Handles transceiver general communications
- [**RHReliableDatagram.h**](https://goo.gl/Ks9rfE): Handles transceiver encrypted packet communications

## Documentation

- Docs have been generated using  [Doxygen](http://www.doxygen.nl/manual/starting.html).
- Configuration is defined in `Doxyfile`.

### Generating or editting the documentation

1. Install  [Doxygen](http://www.doxygen.nl/manual/starting.html).
2. Install [Graphviz](https://www.graphviz.org/download/).
3. Go to Windows [environment variables](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/) and open the `PATH` variable.
4. Add Graphviz's `dot.exe` absolute path (something like `C:\Program Files (x86)\Graphviz2.38\bin`).
5. Modify comment blocks in code as desired.
6. Edit `Doxyfile` configuration file if desired.
7. Run:

```bash
doxygen Doxyfile
```

## Repository structure

|      File      |                                      Description                                      |
| -------------- | ------------------------------------------------------------------------------------- |
| `station.h`    | Defines Station class, macros, and libraries                                          |
| `station.cpp`  | Contains Station class public and private methods                                     |
| `.gitignore`   | Tells Git to [ignore local files](https://git-scm.com/docs/gitignore)                 |
| `keywords.txt` | Tells Arduino to [highlight Station class and main methods](https://goo.gl/F4Xsrk)    |
| `Doxyfile`     | Contains [Doxygen](http://www.doxygen.nl/manual/starting.html) documentation settings |

|          Folder          |                       Description                        |
| ------------------------ | -------------------------------------------------------- |
| :file_folder: `docs`     | Contains resources for Doxygen to generate documentation |
| :file_folder: `examples` | Arduino sketches to be compiled on the Station subsystem |

## Authors

- David Criado Pernía

## References

- [Adafruit **Feather M0 Datasheet**](https://www.adafruit.com/product/3177)
- [Adafruit **Feather M0 Installation Tutorial**](https://learn.adafruit.com/adafruit-feather-m0-radio-with-rfm69-packet-radio)
- [Arduino **RF69 Library**](http://www.airspayce.com/mikem/arduino/RadioHead/classRH__RF69.html)
- [**RFM69HCW Datasheet**](https://fccid.io/ANATEL/01365-15-05508/Manual-para-referencia-RFM69HCW/BFBB2D4A-360A-44F8-B840-F3CF0B535FE6/PDF)