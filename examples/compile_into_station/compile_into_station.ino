/****************************************************************************
 * Air Bearing - Station function suite
 * David Criado Pernia
 * 2019
 * 
 * EXAMPLE
 * Code to be compiled in Station
 *
 * LED BLINK ERROR CODES
 * ==========================================================================
 * 
 *
 ***************************************************************************/

// Include Station library
#include <station.h>

// Declare instance of class Station
Station station = Station();

// Prepare environment
void setup()
{
    // Init Station
    station.begin();
}

// Run infinitely
void loop()
{
    // Listen if any telecommand (TC) has been sent from serial USB
    // A: Operating mode, B & C: Parameters
    station.listen();
    // Send new telecommands and receive telemetry
    station.operate();
}